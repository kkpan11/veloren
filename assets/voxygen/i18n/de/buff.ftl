## Health

buff-title-heal = Heilung
buff-desc-heal = Erhalte mit der Zeit Lebenspunkte.
buff-stat-health = Stellt { $str_total } Lebenspunkte wieder her

## Potion

buff-title-potion = Zaubertrank
buff-desc-potion = Trinke...

## Saturation

buff-title-saturation = Sättigung
buff-desc-saturation = Erhalte mit der Zeit Lebenspunkte durch Nahrung.

## Campfire

buff-title-campfire_heal = Heilung durch Lagerfeuer
buff-desc-campfire_heal = Das Sitzen am Lagerfeuer heilt { $rate }% der Lebensenergie pro Sekunde.

## Energy Regen

buff-title-energy_regen = Ausdauerregeneration
buff-desc-energy_regen = Schnellere Ausdauerregeneration
buff-stat-energy_regen = Stellt { $str_total } Ausdauer wieder her

## Health Increase

buff-title-increase_max_health = Erhöhung der maximalen Lebensenergie
buff-desc-increase_max_health = Erhöhe deine maximale Lebensenergiegrenze
buff-stat-increase_max_health =
    Erhöht die maximale Lebensenergie
    um { $strength }

## Energy Increase

buff-title-increase_max_energy = Erhöhung der maximalen Ausdauer
buff-desc-increase_max_energy = Erhöhe deine maximale Ausdauergrenze
buff-stat-increase_max_energy =
    Erhöht die maximale Ausdauer
    um { $strength }

## Invulnerability

buff-title-invulnerability = Unverwundbarkeit
buff-desc-invulnerability = Du bist immun gegen jeglichen Schaden.
buff-stat-invulnerability = Gewährt Unverwundbarkeit

## Protection Ward

buff-title-protectingward = Schutzaura
buff-desc-protectingward = Du bist einigermaßen vor Angriffen geschützt.

## Frenzied

buff-title-frenzied = Rasend
buff-desc-frenzied = Du bist mit einer unnatürlichen Geschwindigkeit durchtränkt und ignorierst kleinere Verletzungen.

## Haste

buff-title-hastened = Beschleunigend
buff-desc-hastened = Deine Bewegungen und Attacken sind schneller.

## Bleeding

buff-title-bleed = Blutend
buff-desc-bleed = Fügt regelmäßigen Schaden zu.

## Cursed

buff-title-cursed = Verflucht
buff-desc-cursed = Du bist verflucht.

## Burning

buff-title-burn = Brennend
buff-desc-burn = Du verbrennst

## Crippled

buff-title-crippled = Verkrüppelt
buff-desc-crippled = Deine Bewegungen sind eingeschränkt, da deine Beine stark beschädigt sind..

### Freeze

buff-title-frozen = Frierend
buff-desc-frozen = Deine Bewegungen und Attacken sind verlangsamt. Wärme dich auf.

## Wet

buff-title-wet = Nass
buff-desc-wet = Du rutschst über den Boden und kannst kaum anhalten.

## Ensnared

buff-title-ensnared = Gefesselt
buff-desc-ensnared = Äste greifen nach deinen Beinen und verlangsamen deine Bewegungen.

## Util

buff-text-over_seconds = über { $dur_secs } Sekunden
buff-text-for_seconds = für { $dur_secs } Sekunden
buff-remove = Klicke zum Entfernen
# Potion sickness
buff-title-potionsickness = Zaubertrank-Übelkeit
# Potion sickness
buff-desc-potionsickness = Tränke heilen dich weniger nachdem du kurz davor bereits einen konsumiert hast.
# Imminent Critical
buff-desc-imminentcritical = Dein nächster Angriff wird deinen Gegner kritisch treffen.
# Reckless
buff-desc-reckless = Deine Angriffe werden stärker, jedoch lässt du deine Verteidigung offen.
# Agility
buff-desc-agility = Deine Bewegungen sind schneller, aber du verteilst weniger und nimmst mehr Schaden.
# Parried
buff-title-parried = Pariert
# Agility
buff-stat-agility =
    Erhöht Bewegungsgeschwindigkeit um { $strength }%.
    Aber reduziert deinen Schaden um 100%,
    and erhöht deine Schadensverwundbarkeit
    um 100%.
# Lifesteal
buff-desc-lifesteal = Saugt die Leben deiner Feinde aus.
# Fury
buff-desc-fury = Mit deiner Wut werden deine Schläge mehr Combo erzielen
# Lifesteal
buff-title-lifesteal = Lebensraub
# Sunderer
buff-desc-defiance = Du kannst mächtigeren und erschütternderen Schlägen widerstehen und Combo mit jedem Treffer erzielen, jedoch bist du langsamer.
# Flame
buff-title-flame = Flamme
# Polymorped
buff-desc-polymorphed = Dein Körper verändert die Form.
# Salamander's Aspect
buff-desc-salamanderaspect = Du kannst nicht brennen und bewegst dich schnell durch Lava.
# Fury
buff-title-fury = Wut
# Frigid
buff-desc-frigid = Gefriere deine Feinde.
# Sunderer
buff-title-defiance = Trotz
# Flame
buff-desc-flame = Flammen sind deine Verbündeten.
# Potion sickness
buff-stat-potionsickness =
    Reduziert deine Heilung von
    folgenden Tränken um { $strength }%.
# Reckless
buff-title-reckless = Rücksichtslos
# Fortitude
buff-desc-fortitude = Du kannst Taumeln widerstehen, und während du mehr Schaden erleidest, kannst du andere einfacher ins Taumeln bringen.
# Fortitude
buff-title-fortitude = Tapferkeit
# Frigid
buff-title-frigid = Kalt
# Agility
buff-title-agility = Beweglichkeit
# Bloodfeast
buff-title-bloodfeast = Blutbad
# Heatstroke
buff-title-heatstroke = Hitzschlag
# Heatstroke
buff-desc-heatstroke = Du wurdest Hitze ausgesetzt und erleidest nun einen Hitzschlag. Dein Energiebonus und deine Bewegungsgeschwindigkeit sind eingeschränkt. Kühl dich ab.
# Berserk
buff-title-berserk = Berserker
buff-mysterious = Mysteriöser Effekt
# Sunderer
buff-title-sunderer = Zersplitterung
# Polymorped
buff-title-polymorphed = Verwandelt
# Parried
buff-desc-parried = Du wurdest pariert und erholst dich jetzt nur langsam.
# Sunderer
buff-desc-sunderer = Deine Angriffe können die gegnerische Verteidigung durchbrechen und erfrischen dich mit mehr Energie.
# Berserk
buff-desc-berserk = Du bist in einer Berserkerwut, die deine Angriffe stärker und geschickter macht und deine Geschwindigkeit erhöht. Jedoch hast du dadurch geringere Abwehrfähigkeiten.
# Bloodfeast
buff-desc-bloodfeast = Du regenerierst Leben durch Angriffe gegen blutende Gegner
